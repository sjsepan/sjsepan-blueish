# Blueish Color Theme - Change Log

## [0.3.5]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.3.4]

- update readme and screenshot

## [0.3.3]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode
- focusborder to match theme
- define window border in custom mode
- fix menu FG/BG

## [0.3.2]

- fix manifest and pub WF

## [0.3.1]

- match statusbar FG/BG

## [0.3.0]

- dimmer button BG so as to not compete visually with the project names in the Git sidebar
- fix manifest repo links
- retain v0.2.4 for those who prefer the earlier style

## [0.2.4]

- make lst act sel BG transparent

## [0.2.3]

- fix syntax FG contrast on exceptional cases:
illegal
Broken
Deprecated
Unimplemented

## [0.2.2]

- fix scrollbar, minimap slider transparencies:
"minimapSlider.activeBackground": "#00000040",
"minimapSlider.background": "#00000020",
"minimapSlider.hoverBackground": "#00000060",
"scrollbarSlider.activeBackground": "#00000040",
"scrollbarSlider.background": "#00000020",
"scrollbarSlider.hoverBackground": "#00000060",

## [0.2.1]

- fix input border

## [0.2.0]

- retain v0.1.4 for those who prefer the previous style
- use more consistent BG colors throughout

## [0.1.4]

- fix contrast of notification, hover widget FG/BG

## [0.1.3]

- hover widget FG/BG

## [0.1.2]

- notification FG/BG

## [0.1.1]

- tab borders
- scrollbar and widget shadows
- lighten background of editor and general panels, for contrast
- active selection foreground

## [0.1.0]

- update syntax colors similar to humanelike
- use more consistent colors for activity bar, editor widgets (find, command)
- retain most recent older version of the .VSIX for those who prefer, or are accustomed to, the original look.

## [0.0.3]

- scale icon down
- chg icon color-space

## [0.0.2]

- rename pub and add more manifest values

## [0.0.1]

- Initial release
