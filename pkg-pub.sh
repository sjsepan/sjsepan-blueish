#scratchpad...

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish
vsce publish -i ./sjsepan-blueish-0.3.5.vsix
npx ovsx publish sjsepan-blueish-0.3.5.vsix --debug --pat <PAT>