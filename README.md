# Blueish Theme

Blueish light color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-blueish_code.png](./images/sjsepan-blueish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-blueish_codium.png](./images/sjsepan-blueish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-blueish_codedev.png](./images/sjsepan-blueish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-blueish_ads.png](./images/sjsepan-blueish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-blueish_theia.png](./images/sjsepan-blueish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-blueish_positron.png](./images/sjsepan-blueish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/11/2025
